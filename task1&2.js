/////////////////////////////////////////////////////task 2

class Person {
  // name;

  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  describe() {
    console.log(`${this.name} ${this.age} years old
    `);
  }
}

const john = new Person("John", 19);

john.describe();

/////////////////////////////////////////////////////task 2

/////////////////////////////////////////////////////task 1
class Baby extends Person {
  constructor(name, age, favoriteToy) {
    super(name, age);
    this.favoriteToy = favoriteToy;
  }

  play() {
    console.log(`Playing with ${this.favoriteToy}`);
  }
}

const annaBaby = new Baby("Anna", 1, "doll");

annaBaby.play();
/////////////////////////////////////////////////////task 1
